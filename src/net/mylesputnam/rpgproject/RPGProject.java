package net.mylesputnam.rpgproject;

import net.mylesputnam.rpgproject.screens.GameScreen;

import com.badlogic.gdx.Game;

//simply creates the project
public class RPGProject extends Game
{
	@Override
	public void create()
	{
		setScreen(new GameScreen());
	}
}
