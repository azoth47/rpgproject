package net.mylesputnam.rpgproject.gameobjects.battle.attacks;

import net.mylesputnam.rpgproject.gameobjects.battle.AttackRunner;
import net.mylesputnam.rpgproject.gameobjects.battle.animations.AttackAnimation;
import net.mylesputnam.rpgproject.gameobjects.person.Person;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public interface Attack {
	public void execute(Person attacker, Person Victim);
	public TextureRegion getTexture(int index);
	public AttackAnimation getAttackAnimation(Vector2 start, Vector2 end, AttackRunner attackRunner);
	public int getManaDrain();
}