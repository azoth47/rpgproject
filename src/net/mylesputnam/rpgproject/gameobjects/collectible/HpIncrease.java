package net.mylesputnam.rpgproject.gameobjects.collectible;

import net.mylesputnam.rpgproject.gameobjects.displaymessage.DisplayMessage;
import net.mylesputnam.rpgproject.gameobjects.person.Player;
import net.mylesputnam.rpgproject.view.StatsRenderer;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class HpIncrease extends Collectible
{
	private float hpIncreaseAmount;
	
	public HpIncrease(TextureRegion texture, Vector2 position, float hpIncreaseAmount)
	{
		super(texture, position);
		
		this.hpIncreaseAmount = hpIncreaseAmount;
	}

	@Override
	public void execute(Player player)
	{
		player.getStats().changeHp(hpIncreaseAmount);
		DisplayMessage message = new DisplayMessage("             Health increased by " + Float.toString(hpIncreaseAmount), 2f);
		StatsRenderer.get().addMessage(message);
		//play sound here
	}

}
